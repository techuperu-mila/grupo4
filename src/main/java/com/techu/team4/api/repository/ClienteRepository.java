package com.techu.team4.api.repository;

import com.techu.team4.api.models.Cliente;

import java.util.List;

public interface ClienteRepository {
    List<Cliente> findAll();
    public Cliente findOne(String id);
    public Cliente saveCliente(Cliente client);
    public void updateCliente(Cliente client);
    public void deleteCliente(String id);
}
