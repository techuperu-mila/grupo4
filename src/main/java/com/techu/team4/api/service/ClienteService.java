package com.techu.team4.api.service;

import com.techu.team4.api.models.Cliente;

import java.util.List;

public interface ClienteService {
    List<Cliente> findAll();
    public Cliente findOne(String id);
    public Cliente saveCliente(Cliente veh);
    public void updateCliente(Cliente veh);
    public void deleteCliente(String id);
}
